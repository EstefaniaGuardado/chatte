# Chatte #

Extensible Messaging and Presence Protocol (XMPP) is an open XML technology for real-time communication, which powers a wide range of applications including instant messaging, presence and collaboration.

Chatte is a client iOS that enables you to connect to an XMPP for instant messaging with other people like Google G-Talk.

For more information about the project take a look in the actual [repository](https://github.com/EstefaniaGuardado/Chatte) in Github.

### License ###
[Apache Version 2.0](https://github.com/EstefaniaGuardado/Chatte/blob/develop/License)